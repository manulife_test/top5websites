var crypto = require('crypto');

/**
 * UserController
 *
 * @description :: Server-side logic for managing users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {


    welcome: function(req, res) {
        if (req.session.me) {
            var currentdate = req.param('date');
            if (!currentdate) {
                currentdate = '2016-01-06';
            }
            Website.getAllWebsites(
                currentdate,
                function(websites) {
                    sails.log(websites);
                    sails.log(currentdate);
                    return res.view('user/welcome', {
                        websites: websites
                    });
                });
        } else {
            return res.redirect('/');
        }
    },

    /**
     * `UserController.login()`
     */
    login: function(req, res) {
        User.attemptLogin({
            username: req.param('username'),
            password: req.param('password'),
        }, function(err, user) {
            if (err) return res.negotiate(err);
            if (user) {
                req.session.me = user.id;
                if (req.wantsJSON) {
                    return res.ok({ success: true, payload: 'Success' });
                }
                return res.redirect('/welcome');
            } else {
                if (req.wantsJSON) {
                    return res.ok({ success: false, payload: 'User Not Found' });
                }
                return res.redirect('/login');
            }
        });
    },


    /**
     * `UserController.logout()`
     */
    logout: function(req, res) {
        req.session.me = null;
        if (req.wantsJSON) {
            return res.ok('Logged out successfully!');
        }
        return res.redirect('/');
    },

    /**
     * `UserController.signup()`
     */
    signup: function(req, res) {
        User.signup({
            username: req.param('username'),
            password: req.param('password'),
        }, function(err, user) {
            sails.log(err);
            if (err) return res.negotiate(err);
            req.session.me = user.id;
            if (req.wantsJSON) {
                return res.ok({ success: true, payload: 'Success' });
            }
            return res.redirect('/welcome');
        });
    }

};