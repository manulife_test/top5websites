var crypto = require('crypto');
module.exports = {

    tableName: 'users',

    attributes: {
        id: {
            type: 'integer',
            primaryKey: true,
            autoIncrement: true
        },
        username: {
            type: 'string',
            required: true
        },
        password: {
            type: 'string',
            required: true
        }
    },

    /**
     * Create a new user using the provided inputs,
     * but encrypt the password first.
     *
     * @param  {Object}   inputs
     *                     • username    {String}
     *                     • password {String}
     * @param  {Function} cb
     */

    signup: function(inputs, cb) {
        var password = crypto.createHash('md5').update(inputs.password).digest('hex');
        // Create a user
        User.create({
            username: inputs.username,
            password: password
        }).exec(cb);
    },



    /**
     * Check validness of a login using the provided inputs.
     * But encrypt the password first.
     *
     * @param  {Object}   inputs
     *                     • username    {String}
     *                     • password {String}
     * @param  {Function} cb
     */

    attemptLogin: function(inputs, cb) {
        var password = crypto.createHash('md5').update(inputs.password).digest('hex');
        // Load a user
        User.findOne({
                username: inputs.username,
                password: password
            })
            .exec(cb);
    }
};