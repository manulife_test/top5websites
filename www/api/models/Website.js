module.exports = {

    tableName: 'websites',

    attributes: {
        id: {
            type: 'integer',
            primaryKey: true,
            autoIncrement: true
        },
        name: {
            type: 'string',
            required: true
        },
        visits: {
            type: 'integer',
            required: true
        },
        date: {
            type: 'date',
            required: true
        }
    },

    createOrUpdate: function(inputs) {
        Website.findOne({
                name: inputs.name,
                date: inputs.date,
            })
            .exec(function(err, website) {
                if (!website) {
                    Website.create({
                        name: inputs.name,
                        date: inputs.date,
                        visits: inputs.visits,
                    }).exec(function(err, item) {
                        sails.log(err);
                    });
                } else {
                    Website.update({
                            name: inputs.name,
                            date: inputs.date,
                        }, { visits: inputs.visits })
                        .exec(function(err, item) {
                            sails.log(err);
                        });
                }
            });
    },
    getAllWebsites: function(_date, cb) {

        var http = require('http'),
            options = {
                host: "private-1de182-mamtrialrankingadjustments4.apiary-mock.com",
                port: 80,
                path: "/exclusions",
                method: 'GET'
            };
        var webservice_request = http.request(options, function(response) {
            var responseData = '';
            response.setEncoding('utf8');
            response.on('error', function(e) { sails.log(e.message); });
            response.on('data', function(chunk) { responseData += chunk; });
            response.on('end', function() {
                try {
                    var result = JSON.parse(responseData);
                    // User Query now , ORM not work
                    var sql = "SELECT * FROM `websites` WHERE `date` = '" + _date + "'";
                    result.forEach(function(element) {
                        var _filter_str = ''
                        if (element.excludedSince || element.excludedTill) {
                            _filter_str = " and NOT(`name` like '%" + element.host + "' ";
                            if (element.excludedSince) {
                                _filter_str += " AND `date` >= '" + element.excludedSince + "'";
                            }
                            if (element.excludedTill) {
                                _filter_str += " AND `date` <= '" + element.excludedTill + "'";
                            }
                            _filter_str += ")";
                            sql += _filter_str;
                        }
                    });
                    sql += " order by visits DESC limit 5";
                    sails.log(sql);
                    Website.query(sql, [],
                        function(err, items) {
                            sails.log(err);
                            sails.log(items);
                            cb(items);
                        });
                    sails.log(result);
                } catch (e) {
                    sails.log.warn('Could not parse response from options.hostname: ' + e);
                }
            });
        });
        webservice_request.end();

    }
};