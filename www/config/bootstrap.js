/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * http://sailsjs.org/#!/documentation/reference/sails.config/sails.config.bootstrap.html
 */
var chokidar = require('chokidar');
var fs = require('fs');

module.exports.bootstrap = function(cb) {

    // Watch file
    var watcher = chokidar.watch('public/data.csv', { ignored: /[\/\\]\./, persistent: true });
    var watcher_callback = function(path) {
        sails.log('File', path, 'has been changed');
        var parse = require('csv-parse');
        var csvData = [];
        var lines = 0;
        fs.createReadStream(path)
            .pipe(parse({
                delimiter: '|',
                skip_empty_lines: true,
            }))
            .on('data', function(csvrow) {
                csvData.push(csvrow);
            })
            .on('end', function() {
                csvData.forEach(function(record, index) {
                    if (index != 0 && record) {
                        sails.log(record[0]);
                        Website.createOrUpdate({
                            name: record[1],
                            visits: record[2],
                            date: record[0]
                        });
                    }
                });
            });
    }
    watcher
        .on('add', watcher_callback)
        .on('change', watcher_callback);
    // It's very important to trigger this callback method when you are finished
    // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
    cb();
};