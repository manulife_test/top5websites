# Manulife.md
### Instruction

To use it, simply:

* Install Docker and Docker compose in your mac/windows/ubuntu
* Open Terminal 
* In the Terminal ,go into the docker folder
* Then type , docker-compose up -d
* Default website url will appear in your computer : http://127.0.0.1:2337
* To create first user , please go to http://127.0.0.1:2337/signup
* After created the first user, you can login now.

Alan Lam
